<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <title>jQuery・Ajax・Cake</title>
    <?php
    echo $this->Html->script('http://code.jquery.com/jquery-1.11.3.min.js');
    ?> 
</head>
<body>
    <h1>jQuery・Ajax</h1>
    <?php 
    echo $this->Form->control("Q1",array('id'=>'Q1'));
    echo $this->Form->submit('Ajax',['id' => 'send']);
    $this->Form->end();
    ?>
</body>
</html>
<script>
$('document').ready(function(){
    $('#send').click(function()
    {
        var data = { request : $('#Q1').val() };
        $.ajax({
            type: 'POST',
            datatype:'json',
            url: "<?php echo $this->Url->build( [ 'controller' => 'Users', 'action' => 'result' ] ); ?>",
            data: data,
            success: function(data,dataType)
            {         
                alert('Success');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + errorThrown);
            }
        });
        return false;
    });
});

</script>
