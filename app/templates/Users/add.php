<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4> 
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
        
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
        <?= $this->Form->create($user) ?>
            <fieldset>
                <?php
                    echo $this->Form->control('email');
                    echo $this->Form->control('password');
                    echo $this->Form->control('phone');   
                    echo $this->Form->label('Gender');
                    echo $this->Form->radio('gender', ['Male', 'Female']);
                    echo $this->Form->label('Day of Birth');
                    echo $this->Form->date('dob');                 
                    echo $this->Form->label('Postal Code');
                    echo $this->Form->input('postalcode');
                    echo $this->Form->control('address'); 
                ?>
            </fieldset>   
            <?= $this->Form->button(__('Registry')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
