<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4> 
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
        <?= $this->Form->create($ques) ?>
            <fieldset>
        <?php
        echo $this->Form->input('id', array('type' => 'hidden', 'value'=>$id));
        echo $this->Form->control('q1',['label'=>'あなたの好きな動物','type'=>'text']);
        echo $this->form->label('あなたの好きな食べ物');
        echo $this->Form->select('q2',['アイスクリーム','ケーキ','シュークリーム','クレープ'],['empty'=>'(choose one)']);
        echo $this->Form->label('どちらの方が好きですか');
        echo $this->Form->radio('q3',['猫','犬']);
       ?>
        </fieldset>   
            <?= $this->Form->button(__('回答')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
