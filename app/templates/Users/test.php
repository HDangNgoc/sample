<!DOCTYPE html>
<html>
<head>
<?php
    // Ajax 送信用の JavaScript を読み込み
    echo $this->Html->script('http://code.jquery.com/jquery-1.11.3.min.js');
?> 
</head>
<body>
<div class="table-content">
    <table>
        <thead>
        <tr>
                <th scope="col"><?= $this->Paginator->sort('Id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Q1') ?></th>
        </tr>
        </thead>
        <tbody>
            <?php foreach($users as $user): ?>
                <tr>
                <td><?= $this->Number->format($user->id) ?></td>
                <td><p id="<?=$this->Number->format($user->id) ?>" data-id="<?=$this->Number->format($user->id) ?>"><?= h($user->Q1) ?></p></td>
                </tr>
            <?php endforeach;?>
        </tbody>
        </table>
    </div>
    <div class="paginator">
            <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('first')) ?>
                <?= $this->Paginator->prev('< ' . __('previous')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('next') . ' >') ?>
                <?= $this->Paginator->last(__('last') . ' >>') ?>
            </ul>
    </div>
</body>
<footer>
<script>
$('document').ready(function(){
$("p").dblclick(function(){
    var data_id = $(this).data('id');  
        Users( data_id );
         });
        function Users( keyword ){
        var data = keyword;
        var id = $(this).attr('id');
        $.ajax({
                    method: 'GET',
                    url : "<?php echo $this->Url->build( [ 'controller' => 'Users', 'action' => 'info' ] ); ?>",
                    data: {keyword:data},
                    success: function( response )
                    {   
                        $("#"+data).html(response);
                    }
                });
        };
    });
</script>
</footer>
</html>
