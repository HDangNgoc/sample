<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <?php
    echo $this->Html->script('http://code.jquery.com/jquery-1.11.3.min.js');
    ?> 
</head>
<body>
    <?php 
    echo $this->Form->input("id",array('id'=>'id','type'=>'hidden','value'=>$id));
    echo $this->Form->input("Q1",array('id'=>'Q1'));
    echo $this->Form->submit('Ajax',['id' => 'send']);
    $this->Form->end();
    ?>
</body>
</html>
<script>
$('document').ready(function(){
    $('#send').click(function()
    {   var data = { id : $('#id').val(), request : $('#Q1').val() };
        $.ajax({
            type: 'GET',
            datatype:'json',
            url: "<?php echo $this->Url->build([ 'controller' => 'Users', 'action' => 'update' ]); ?>",
            data: data,
            success: function(data,dataType)
            {         
                alert('Success');
            },
            error: function(XMLHttpRequest, textStatus, errorThrown)
            {
                alert('Error : ' + errorThrown);
            }
        });
        return false;
    });
});
</script>
