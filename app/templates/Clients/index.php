<html>
   <head>
   <style>
   body {
  padding: 10px;
  font-size: 0.75rem;
  background: #20262e;
}
#input {
  padding: 20px;
  background: #fff;
}
input {
  border: 1px solid #848484;
	border-radius: 30px;
	outline:0;
	height:25px;
	width: 275px;
	padding-left:10px;
	padding-right:10px;
}

#table {
  padding: 20px;
  background: #fff;
  border-radius: 4px;
  transition: all 0.2s;
}
table {
  width: 100%;
  border: 1px solid #ccc;
  border-collapse: collapse;
}
th,
td {
  padding: 5px;
  border: 1px solid #ccc;
}

thead tr {
  color: #fff;
  background-color: #000;
}

tbody tr:nth-of-type(odd) {
  background-color: silver;
}
   </style>
      <?php
      echo $this->Html->script('vue');
      ?>
      <script src="https://cdn.jsdelivr.net/npm/vue-burger-menu@2.0.3/dist/vue-burger-menu.umd.js"></script>
   </head>
   <body style="background-image: url('https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1534&q=80');">
    <div id = "input">
         Email : <input type = "text" v-model = "email" /> <br/><br/>
         Client : <input type = "text" v-model = "client"/> <br/><br/>
         <p style= "font-size: 30px;" >Input Text:<p>
         <h1>Email : {{email}} </h1>
         <h1>Client : {{client}}</h1>
    </div>
   <div id="table">
  <table>
    <thead>
      <tr>
        <th>ID</th>
        <td>Email</td>
        <td>Client</td>
        <td>Age</td>
        <td>Phone number</td>
      </tr>
    </thead>
    <tbody>
      <tr v-for="data in datas" :key="data.id">
        <th>{{ data.id }}</th>
        <td>{{ data.email }}</td>
        <td>{{ data.client }}</td>
        <td>{{ data.age }}</td>
        <td>{{ data.phone }}</td>
      </tr>
    </tbody>
  </table>
</div>
<script>
            new Vue({
  el: '#table',
  data: {
    datas: [
      {
        id: 1,
        email: 'dangngoc@la-j.com',
        client: 'ダン',
        age: '23',
        phone: '08011111',
      },
      {
        id: 2,
        email: 'dangngoc2@la-j.com',
        client: 'ダン2',
        age: '24',
        phone: '0802222',
      },
      {
        id: 3,
        email: 'dangngoc3@la-j.com',
        client: 'ダン3',
        age: '25',
        phone: '0803333',
      },
      {
        id: 4,
        email: 'dangngoc4@la-j.com',
        client: 'ダン4',
        age: '26',
        phone: '08044444',
      }
        ]
       }
      });
         
         var vm = new Vue({
         el: '#input',
         data: {
          
         email :"",
         client :""
          },
})
      </script>
   </body>
</html>