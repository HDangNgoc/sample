<?php
echo $this->fetch('content')
?>
<script type="text/javascript">

$(function(){
    // Ajax button click
    $('#ajax').on('click',function(){
        $.ajax({
            type:'POST',
            url:'users/survey',
            data:{
                'Q1':$('#Q1').val(),
                'Q2':$('#Q2').val()
            }
        })
        .done( (data) => {
            $('.result').html(data);
            console.log('Ajax通信に成功しました');
            console.log(data);
        })
        .fail( (data) => {
            $('.result').html(data);
            console.log('Ajax通信に失敗しました');
            console.log(data);
        })
    });
});

</script>