<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class CsvForm extends Form
{
    protected function _buildSchema(Schema $schema): Schema
    {
        return $schema->addField('csv', ['type' => 'file']);
    }

    protected function _buildValidator(Validator $validator): Validator
    {
        
        $validator->add('csv', 'fileFileUpload', [
            'rule' => ['isFileUpload'],
            'message' => 'アップロードに失敗しました。',
            'provider' => 'upload'
        ])->add('csv', 'fileBelowMaxSize', [
            'rule' => ['isBelowMaxSize', 10240000],
            'message' => 'ファイルサイズが大きすぎます。',
            'provider' => 'upload'
        ])->add('csv', 'fileType', [
            'rule' => ['mimeType', ['text/plain']],
            'message' => 'ファイル形式が違います。'
        ]);
    }

    protected function _execute(array $data): bool
    {
        return true;
    }

}