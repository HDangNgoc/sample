<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

class QuestionareTable extends Table
{
public function initialize(array $config): void
{
    parent::initialize($config);

    $this->setTable('questionare');
    $this->setDisplayField('id');
    $this->setPrimaryKey('id');
    $this->addBehavior('Timestamp');   
    $this->belongsTo('Users');
}
public function validationDefault(Validator $validator): Validator
{
    $validator
        ->integer('id')
        ->allowEmptyString('id', null, 'create');
    $validator    
        ->maxLength('q1', 255)
        ->requirePresence('q1', 'create')
        ->notEmptyString('q1');
    $validator
        ->requirePresence('q2','create')
        ->notemptystring('q2');
    $validator
        ->requirePresence('q3','create')
        ->notemptystring('q3');
    return $validator;
}
}