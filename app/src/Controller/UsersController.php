<?php
declare(strict_types=1);

namespace App\Controller;
use App\Form\CsvForm; // モデルなしフォームロード
use Cake\Filesystem\Folder;  // Folderユーティリティロード
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
use Cake\ORM\TableRegistry;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 
 */

class UsersController extends AppController

{   //public $paginate = ['limit' => 3];
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        // Configure the login action to not require authentication, preventing
        // the infinite redirect loop issue
        $this->Authentication->addUnauthenticatedActions(['login', 'add','index','ques','count']);
       // if($this->request->action == 'data'){
       //     $this->getEventManager()->off($this->Csrf);
       // }
       
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }
    public function login() {
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();
        // regardless of POST or GET, redirect if user is logged in
        if ($result->isValid()) {
            // redirect to /articles after login success
            $redirect = $this->request->getQuery('redirect', [
                'controller' => 'Users',
                'action' => 'index',
            ]);   
                
            return $this->redirect($redirect);
            
        }

        // display error if user submitted and authentication failed
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }
    public function ques()
    {   $this->loadModel('questionare');
        
        $ques=$this->questionare->newEmptyEntity();
        $this->set('ques',$ques);

        $session = $this->getRequest()->getSession();
        $user_id = $session->read('Auth.id');
        $this->set('id',$user_id);

        if ($this->request->is('post')) {
            $ques = $this->questionare->patchEntity($ques, $this->request->getData());
            if ($this->questionare->save($ques)) 
            {
                $this->Flash->success(__('Your answers has been submitted.'));

                return $this->redirect(['action' => 'index', 'controller' => 'Users']);
            }
            $this->Flash->error(__('The answer could not be saved. Please, try again.'));
        }
        $this->set(compact('QuestionareEntity'));
       
    }
    public function count()
    {   
        $this->loadModel('questionare');
        $query = $this->questionare->find();
        
        $q2_0 = $query->newExpr()
        ->addCase(
        $query->newExpr()->add(['q2' => '0']));
        
        $q2_1 = $query->newExpr()
        ->addCase(
        $query->newExpr()->add(['q2' => '1']));
        
        $q2_2 = $query->newExpr()
        ->addCase(
        $query->newExpr()->add(['q2' => '2']));
        
        $q2_3 = $query->newExpr()
        ->addCase(
        $query->newExpr()->add(['q2' => '3']));

        $query->select([
        'count_q2_0' => $query->func()->count($q2_0),
        'count_q2_1' => $query->func()->count($q2_1),
        'count_q2_2' => $query->func()->count($q2_2),
        'count_q2_3' => $query->func()->count($q2_3)]);
        $this->set('cnt',$query);

        $query2 = $this->questionare->find();

        $q3_0 = $query2->newExpr()
        ->addCase(
        $query2->newExpr()->add(['q3' => '0']));

        $q3_1 = $query2->newExpr()
        ->addCase(
        $query2->newExpr()->add(['q3' => '1']));
        
        $query2->select([
        'count_q3_0' => $query2->func()->count($q3_0),
        'count_q3_1' => $query2->func()->count($q3_1)]);
        $this->set('cnt2',$query2);  
        
        $Title2=array('アイスクリーム',',','ケーキ',',','シュークリーム',',','クレープ');
        $Title2 = mb_convert_encoding($Title2, "SJIS-win","UTF-8");
        $this->set('Q2',$Title2);
        
        $Title3=array('猫',',','犬');
        $Title3 = mb_convert_encoding($Title3, "SJIS-win","UTF-8");
        $this->set('Q3',$Title3);
        
        $this->set('title', 'CsvLayout');
        $this->viewBuilder()->setLayout('CsvLayout');
    }
    
    public function index()
    {   $key = $this->request->getQuery('key');
        if($key){
            $queryA = $this->Users->find('all')
                                  ->where(['OR'=>['address like'=>'%'.$key.'%','email like'=>'%'.$key.'%']]);
        }else{
            $queryA = $this->Users;
        }


        $users = $this->paginate($queryA);
        $this->set(compact('users'));
        $query =  $this->Users->find()
        ->join([
            'table' => 'questionare',
            'alias' => 'c',
            'type' => 'INNER',
            'conditions' => 'c.id = Users.id',
        ])
        ->select([
            'id' => 'Users.id',
            'email'=>'users.email',
            'q1'=> 'c.q1',
            'q2'=>'c.q2',
            'q3'=>'c.q3',
        ]);
        $this->set('id', $query); 
        
        }
    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
     public function add()
    {       $user = $this->Users->newEmptyEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
    
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));

            
        
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function logout()
{
    $result = $this->Authentication->getResult();
    // regardless of POST or GET, redirect if user is logged in
    if ($result->isValid()) {
        $this->Authentication->logout();
        return $this->redirect(['controller' => 'Users', 'action' => 'login']);
    $this->Flash->success(__('You are loogged out'));
    }

}

    public function csvImport(){
    
    $this->loadModel('Articles');
    $tmp_file_name = $_FILES['"csv"']["tmp_name"];
    $data=array();
    $articles = $this->Articles->newEntities($data);
    if (is_uploaded_file($tmp_file_name)){
        $csv_contents = fopen($tmp_file_name, "r");
        fgetcsv($csv_contents);
        while(($csv_content = fgetcsv($csv_contents, 0, ",")) !==FALSE){
            $datas = mb_convert_encoding($csv_content, 'UTF-8', "auto");  
            $temp['id'] = $datas[0];
            $temp['address'] = $datas[1];
            $temp['postalcode'] = $datas[2];
            $data[]=$temp; 
        }
    };
    $query = $this->Articles->query('SELECT id from articles WHERE id = '.$temp['id'].'');
    if(isset($query))
    {
        $articles=$this->Articles->PatchEntities($articles,$data);  
    }
    else
    {
        $articles = $this->Articles->newEntities($data);
    };
    if($this->Articles->SaveMany($articles)){
        $this->Flash->success(__('CSVファイルを追加しました'));
        print_r ($data);
    } 
   // else
   // {
   //     $this->Flash->error(__('CSVファイルを追加できませんでした'));
   // };
    $this->set('articles',$articles); 
    
}
    public function survey()
    { 
        $this->loadModel('Survey');      
    }
    public function result()
    {
        $this->loadModel('Survey');
        $data = $this->request->getdata('request');
        $connection = ConnectionManager::get('default');
        $connection->insert('survey', [ 'Q1' => $data ]);
    }
    public function data()
    {   
        $this->loadModel('survey');
        $users = $this->paginate($this->survey);
        $this->set(compact('users'));
    }
    public function search()
    {   
        $this->loadModel('Survey');
        $keyword = $this->request->getquery('keyword');
        $query = $this->Survey->find('all',[
              'conditions' => ['Q1 LIKE'=>'%'.$keyword.'%'],
              'order' => ['Survey.id'=>'ASC'],    
        ]);
        $this->set('users', $this->paginate($query));
        $this->set('_serialize', ['users']);

    }   
    public function test()
    {
        $this->loadModel('survey');
        $users = $this->paginate($this->survey);
        $this->set(compact('users'));
    }
    public function info()
    {   
        $this->loadModel('survey');
        $id=$this->request->getquery('keyword');
        $this->set('id',$id);
        
    }
    public function update()
    {   $this->viewBuilder()->setLayout('ajax');
        $this->loadModel('survey');
        $data_request = $this->request->getQuery('request');
        $data_id = $this->request->getQuery('id');
        $data=array();
        $datas=array('id'=>$data_id, 'Q1'=>$data_request);
        $entity = $this->survey->NewEntity($data);
        $query = $this->survey->query('SELECT id from survey WHERE id = '.$data_id.'');
    if(isset($query))
    {
        $data=$datas;
        $result = $this->survey->patchEntity($entity,$data); 

    };
        $this->survey->save($result);
    }
}






