<?php
namespace App\Mailer;

use Cake\Mailer\Mailer;

class ClientMailer extends Mailer
{
    public function ques($ques)
    {
        $this
            ->setTo('dangngoc@la-j.com')
            ->setSubject('Test')
            ->setemailFormat("text")
            ->viewBuilder()
                ->setTemplate('ques_mail')
                ->setLayout('default')
                ->setVars(['client' => $ques]);

    }
}