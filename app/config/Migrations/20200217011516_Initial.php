<?php
declare(strict_types=1);

use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public $autoId = false;

    public function up()
    {
       // $this->table('users')
          //  ->addColumn('id', 'integer', [
         //       'autoIncrement' => true,
         //       'default' => null,
         //       'limit' => 11,
          //      'null' => false,
           //     'signed' => false,
           // ])
           // ->addPrimaryKey(['id'])
           // ->addColumn('username', 'string', [
           //     'default' => null,
             //   'limit' => 50,
               // 'null' => false,
         //   ])
           // ->addColumn('password', 'string', [
             //   'default' => null,
               // 'limit' => 100,
        //        'null' => false,
         //   ])
         //   ->addIndex(
         //       [
          //          'username',
         //       ]
          //  )
         //   ->create();

        $this->table('users2')
            ->addColumn('id', 'integer', [
                'autoIncrement' => true,
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addPrimaryKey(['id'])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('phone', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->create();
    }

    public function down()
    {
        //$this->table('users')->drop()->save();
        $this->table('users')->drop()->save();
    }
}
