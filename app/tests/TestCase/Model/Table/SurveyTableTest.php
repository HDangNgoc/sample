<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SurveyTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SurveyTable Test Case
 */
class SurveyTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SurveyTable
     */
    protected $Survey;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Survey',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Survey') ? [] : ['className' => SurveyTable::class];
        $this->Survey = TableRegistry::getTableLocator()->get('Survey', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Survey);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
