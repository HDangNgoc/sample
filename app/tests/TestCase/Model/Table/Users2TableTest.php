<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\Users2Table;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\Users2Table Test Case
 */
class Users2TableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\Users2Table
     */
    protected $Users2;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Users2',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Users2') ? [] : ['className' => Users2Table::class];
        $this->Users2 = TableRegistry::getTableLocator()->get('Users2', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Users2);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
