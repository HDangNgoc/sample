<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ExtraTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ExtraTable Test Case
 */
class ExtraTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ExtraTable
     */
    protected $Extra;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Extra',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Extra') ? [] : ['className' => ExtraTable::class];
        $this->Extra = TableRegistry::getTableLocator()->get('Extra', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Extra);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
